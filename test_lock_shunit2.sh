#!/bin/sh
# Bourne shell unit tests using shunit2
# installed on Debian Stretch using apt (2.1.6-1.1)
#
# These are tests that don't take too long to run.

# load lock procedures (if needed)
. ./lock.sh

# test that script acquires resource, holds for $HOLD seconds and
# releases it. HOLD needs to be <= 2 since there is 1 s slop in the
# comparison
hold=2
LOCK_NAME="lock_test"

# Verify that the lock is acquired and held for the desired length of time
testAcquireRelease() {
    start=`date +%s`
    shouldfin=`expr $start + $hold`
    ./test_lock.sh $LOCK_NAME 1 $hold || fail "cannot acquire /tmp/lock_test/pid"
    fin=`date +%s`

    # verify that test_lock.sh held the lock the desired time
    lower_limit=`expr $shouldfin - 1`
    upper_limit=`expr $shouldfin + 1`
    assertTrue "hold time not in limit of $lower_limit to $upper_limit"\
        "[ $fin -ge $lower_limit ] && [ $fin -le $upper_limit ]"
    
    # verify that test_lock.sh
    assertFalse "lock file not removed" "[ -e /tmp/$LOCK_NAME/pid ]"
 }


# Acquire a lock and modify the PID then verify it cannot be released.
testCannotRelease() {
    acquireLock $LOCK_NAME 1 || fail "Cannot acquire /tmp/$LOCK_NAME/pid"

    echo 0 > /tmp/$LOCK_NAME/pid # corrupt PID file

    if $(releaseLock $LOCK_NAME) ; then
        fail "released lock in error"
    fi
    rm -rf /tmp/$LOCK_NAME
    assertTrue :
}

LOCK_NAME="dead_lock"

# test can acquire lock
testAcquireDeadLock() {
    # grab a lock in another process
    sh ./grab_lock.sh $LOCK_NAME 1 || fail "Can't acquire lock to start with"
    sh ./grab_lock.sh $LOCK_NAME 1 || fail "Can't acquire lock held by dead process"

    rm -rf /tmp/$LOCK_NAME
}

# Verify that a dead lock can be  acquired 
# Hmmm... test already there. ;)
#
testAcquireDeadLockAgain() {

    ./grab_lock.sh ${LOCK_NAME} 0  # creates dead lock
    OLD_LOCK=`cat /tmp/${LOCK_NAME}/pid`
    ./grab_lock.sh ${LOCK_NAME} 0  || fail "didn't acquire lock"

    # trust but verify
    if  [ $OLD_LOCK -eq `cat /tmp/${LOCK_NAME}/pid` ]
    then
        fail "same PID both locks"
    fi
    rm -rf  /tmp/${LOCK_NAME}
 }

# Verify that a R/O dead lock is not acquired and is reported
# 
testCantAcquireReadonlyDeadLock() {

    ./grab_lock.sh ${LOCK_NAME} 0  # creates deac locl
    OLD_LOCK=`cat /tmp/${LOCK_NAME}/pid`
    # make sure next attempt fails
    chmod -w  /tmp/${LOCK_NAME}/pid
    ./grab_lock.sh ${LOCK_NAME} 0 2>/dev/null && fail "acquired lock in error"
    

    rm -rf  /tmp/${LOCK_NAME}
 }



# Load shUnit2.
. shunit2
